<?php
/**
 * woocommerce-mtn-gateway Plugin is the simplest WordPress plugin for beginner.
 * Take this as a base plugin and modify as per your need.
 *
 * @package woocommerce-kanzu-min-max-plugin
 * @author woocommerce-kanzu-min-max-plugin
 * @license GPL-2.0+
 * @link https://woocommerce-kanzu-min-max-plugin.com/tag/wordpress-beginner/
 * @copyright 2017 Kasozi, LLC. All rights reserved.
 *
 *            @wordpress-plugin
 *            Plugin Name: woocommerce-kanzu-min-max-plugin
 *            Plugin URI: https://woocommerce-kanzu-min-max-plugin.com/tag/wordpress-beginner/
 *            Description: woocommerce-kanzu-min-max-plugin
 *            Version: 3.0
 *            Author: Kasozi
 *            Author URI: https://woocommerce-kanzu-min-max-plugin.com/
 *            Text Domain: woocommerce-kanzu-min-max-plugin
 *            Contributors: woocommerce-kanzu-min-max-plugin
 *            License: GPL-2.0+
 *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

if (!defined('ABSPATH')) {
    die;
}


/**
 * Register the custom product type after init
 */
function register_simple_event_product_type()
{

    /**
     * This should be in its own separate file.
     */
    class WC_Product_Simple_Event extends WC_Product_Simple
    {
        public function __construct($product)
        {
            $this->product_type = 'simple_event';

            parent::__construct($product);
        }
    }
}
add_action('init', 'register_simple_event_product_type');

function add_simple_event_product($types)
{

    // Key should be exactly the same as in the class product_type parameter
    $types['simple_event'] = __('Kanzu Event');

    return $types;
}
add_filter('product_type_selector', 'add_simple_event_product');

/**
 * Show pricing fields for simple_rental product.
 */
function simple_event_custom_js()
{
    if ('product' != get_post_type()) :
        return;
    endif; ?><script type='text/javascript'>
        jQuery(document).ready(function() {
            jQuery('.options_group.pricing').addClass('show_if_simple_event').show();
            jQuery('.inventory_options').addClass('show_if_simple_event').show();
            jQuery('#inventory_product_data ._manage_stock_field').addClass('show_if_simple_event').show();
            jQuery('#inventory_product_data ._sold_individually_field').parent().addClass('show_if_simple_event').show();
            jQuery('#inventory_product_data ._sold_individually_field').addClass('show_if_simple_event').show();
        });
    </script><?php
        }
        add_action('admin_footer', 'simple_event_custom_js');

        //am good up to here

        /**
         * Add a custom product tab.
         */
        function custom_product_tabs($tabs)
        {
            $tabs['event'] = array(
                'label'        => __('Kanzu Event', 'woocommerce'),
                'target'    => 'event_options',
                'class'        => array('show_if_simple_event', 'show_if_variable_event'),
            );
            return $tabs;
        }
        add_filter('woocommerce_product_data_tabs', 'custom_product_tabs');
        /**
         * Contents of the rental options product tab.
         */
        function event_options_product_tab_content()
        {
            global $post; ?><div id='event_options' class='panel woocommerce_options_panel'>
        <?php
        ?><div class='options_group'>
            <?php
            woocommerce_wp_text_input(array(
                'id'            => 'txt_start_date',
                'label'            => __('Start Date of Event', 'woocommerce'),
                'desc_tip'        => 'true',
                'description'    => __('The Date when the Event Starts', 'woocommerce'),
                'type'             => 'date',
            ));

            woocommerce_wp_text_input(array(
                'id'            => 'txt_end_date',
                'label'            => __('Event Date of Event', 'woocommerce'),
                'desc_tip'        => 'true',
                'description'    => __('The Date when the Event Ends', 'woocommerce'),
                'type'             => 'date',
            )); ?></div>

    </div><?php
    }
    add_action('woocommerce_product_data_panels', 'event_options_product_tab_content');

    /**
     * Save the custom fields.
     */
    function save_event_option_field($post_id)
    {
        $start_date =  isset($_POST['txt_start_date']) ? $_POST['txt_start_date'] : date("m-d-Y");
        $end_date =  isset($_POST['txt_end_date']) ? $_POST['txt_end_date'] : date("m-d-Y");
        $start_date = empty($start_date) ? date_create()->format('m-d-Y') : $start_date;
        $end_date = empty($end_date) ? date_create('+1 day')->format('m-d-Y') : $end_date;

        WriteToLog("StartDate: $start_date");
        WriteToLog("EndDate: $end_date");

        update_post_meta($post_id, 'txt_start_date', sanitize_text_field($start_date));
        update_post_meta($post_id, 'txt_end_date', sanitize_text_field($end_date));
    }
    add_action('woocommerce_process_product_meta_simple_event', 'save_event_option_field');
    add_action('woocommerce_process_product_meta_variable_event', 'save_event_option_field');

    /**
     * Hide Attributes data panel.
     */
    function hide_attributes_data_panel($tabs)
    {

        // Other default values for 'attribute' are; general, inventory, shipping, linked_product, variations, advanced
        $tabs['attribute']['class'][] = 'hide_if_simple_rental hide_if_variable_rental';

        return $tabs;
    }
    add_filter('woocommerce_product_data_tabs', 'hide_attributes_data_panel');

    /**
     * Display Inventory tabs to do with MIN and Max
     */

    function wc_qty_add_product_field()
    {
        echo '<div class="options_group">';
        woocommerce_wp_text_input(
            array(
                'id'          => '_wc_min_qty_product',
                'label'       => __('Minimum Quantity', 'woocommerce-max-quantity'),
                'placeholder' => '',
                'desc_tip'    => 'true',
                'description' => __('Optional. Set a minimum quantity limit allowed per order. Enter a number, 1 or greater.', 'woocommerce-max-quantity')
            )
        );
        echo '</div>';
        echo '<div class="options_group">';
        woocommerce_wp_text_input(
            array(
                'id'          => '_wc_max_qty_product',
                'label'       => __('Maximum Quantity', 'woocommerce-max-quantity'),
                'placeholder' => '',
                'desc_tip'    => 'true',
                'description' => __('Optional. Set a maximum quantity limit allowed per order. Enter a number, 1 or greater.', 'woocommerce-max-quantity')
            )
        );
        echo '</div>';
    }
    add_action('woocommerce_product_options_inventory_product_data', 'wc_qty_add_product_field');

    //Save the min and max in posts meta
    function wc_qty_save_product_field($post_id)
    {
        $val_min = trim(get_post_meta($post_id, '_wc_min_qty_product', true));
        $new_min = sanitize_text_field($_POST['_wc_min_qty_product']);
        $val_max = trim(get_post_meta($post_id, '_wc_max_qty_product', true));
        $new_max = sanitize_text_field($_POST['_wc_max_qty_product']);

        if ($val_min != $new_min) {
            update_post_meta($post_id, '_wc_min_qty_product', $new_min);
        }
        if ($val_max != $new_max) {
            update_post_meta($post_id, '_wc_max_qty_product', $new_max);
        }
    }
    add_action('woocommerce_process_product_meta', 'wc_qty_save_product_field');

    /*
* Setting minimum and maximum for quantity input args.
*/
    function wc_qty_input_args($args, $product)
    {
        $product_id = $product->get_parent_id() ? $product->get_parent_id() : $product->get_id();

        $product_min = wc_get_product_min_limit($product_id);
        $product_max = wc_get_product_max_limit($product_id);
        if (!empty($product_min)) {
            // min is empty
            if (false !== $product_min) {
                $args['min_value'] = $product_min;
            }
        }
        if (!empty($product_max)) {
            // max is empty
            if (false !== $product_max) {
                $args['max_value'] = $product_max;
            }
        }
        if ($product->managing_stock() && !$product->backorders_allowed()) {
            $stock = $product->get_stock_quantity();
            $args['max_value'] = min($stock, $args['max_value']);
        }
        return $args;
    }
    add_filter('woocommerce_quantity_input_args', 'wc_qty_input_args', 10, 2);

    function wc_get_product_max_limit($product_id)
    {
        $qty = get_post_meta($product_id, '_wc_max_qty_product', true);
        if (empty($qty)) {
            $limit = false;
        } else {
            $limit = (int)$qty;
        }
        return $limit;
    }

    function wc_get_product_min_limit($product_id)
    {
        $qty = get_post_meta($product_id, '_wc_min_qty_product', true);
        if (empty($qty)) {
            $limit = false;
        } else {
            $limit = (int)$qty;
        }
        return $limit;
    }

    /*
* Validating the quantity on add to cart action with the quantity of the same product available in the cart. 
*/
    function wc_qty_add_to_cart_validation($passed, $product_id, $quantity, $variation_id = '', $variations = '')
    {
        $product_min = wc_get_product_min_limit($product_id);
        $product_max = wc_get_product_max_limit($product_id);
        if (!empty($product_min)) {
            // min is empty
            if (false !== $product_min) {
                $new_min = $product_min;
            } else {
                // neither max is set, so get out
                return $passed;
            }
        }
        if (!empty($product_max)) {
            // min is empty
            if (false !== $product_max) {
                $new_max = $product_max;
            } else {
                // neither max is set, so get out
                return $passed;
            }
        }
        $already_in_cart     = wc_qty_get_cart_qty($product_id);
        $product             = wc_get_product($product_id);
        $product_title         = $product->get_title();

        if (!is_null($new_max) && !empty($already_in_cart)) {

            if (($already_in_cart + $quantity) > $new_max) {
                // oops. too much.
                $passed = false;
                wc_add_notice(
                    apply_filters(
                        'isa_wc_max_qty_error_message_already_had',
                        sprintf(
                            __('You can add a maximum of %1$s %2$s\'s to %3$s. You already have %4$s.', 'woocommerce-max-quantity'),
                            $new_max,
                            $product_title,
                            '<a href="' . esc_url(wc_get_cart_url()) . '">' . __('your cart', 'woocommerce-max-quantity') . '</a>',
                            $already_in_cart
                        ),
                        $new_max,
                        $already_in_cart
                    ),
                    'error'
                );
            }
        }
        return $passed;
    }
    add_filter('woocommerce_add_to_cart_validation', 'wc_qty_add_to_cart_validation', 1, 5);

    /*
* Get the total quantity of the product available in the cart.
*/
    function wc_qty_get_cart_qty($product_id)
    {
        global $woocommerce;
        $running_qty = 0; // iniializing quantity to 0
        // search the cart for the product in and calculate quantity.
        foreach ($woocommerce->cart->get_cart() as $other_cart_item_keys => $values) {
            if ($product_id == $values['product_id']) {
                $running_qty += (int)$values['quantity'];
            }
        }
        return $running_qty;
    }

    add_action('woocommerce_before_add_to_cart_button', 'add_cf_before_addtocart_in_single_products', 1, 0);

    function add_cf_before_addtocart_in_single_products()
    {

        global $product;

        $start_date = get_post_meta($product->get_id(), 'txt_start_date', true);
        $end_date = get_post_meta($product->get_id(), 'txt_end_date', true);

        if (!empty($start_date)) {
            echo '<div class="pd-number">Start Date: #' . $start_date . '</div><br>';
            echo '<div class="pd-number">End Date: #' . $end_date . '</div><br>';
        }
    }

    /* 
Validate product type before publish
*/
    add_action('wp_print_scripts', 'my_publish_admin_hook');

    function my_publish_admin_hook()
    {
        if (is_admin()) {
            ?>
        <script language="javascript" type="text/javascript">
            jQuery(document).ready(function() {

                //take over the publish button so we can validate our stuff
                jQuery('#publish').click(function(event) {

                    event.preventDefault();

                    var get_data = '';
                    get_data += 'txt_end_date1=' + $("#txt_end_date").val();
                    get_data += '&txt_start_date1=' + $("#txt_start_date").val();
                    get_data += '&product_type1=' + $("#product-type").val();

                    var data = {
                        action: 'my_pre_submit_validation',
                        security: '<?php echo wp_create_nonce('pre_publish_validation'); ?>'
                    };

                    var fullajaxurl = ajaxurl + "?" + get_data;
                    console.log("AjaxURL: " + fullajaxurl);

                    jQuery.get(fullajaxurl, data, function(response) {
                        console.table(response);
                        if (response.success) {
                            jQuery('#ajax-loading').hide();
                            jQuery('#publish').removeClass('button-primary-disabled');
                            jQuery('#post').submit();
                            return true;
                        } else {
                            alert(response.data.message);
                            jQuery('#ajax-loading').hide();
                            jQuery('#publish').removeClass('button-primary-disabled');
                            return false;
                        }
                    });
                });
            });
        </script>
    <?php
}
}

add_action('wp_ajax_my_pre_submit_validation', 'pre_submit_validation');

function pre_submit_validation()
{
    //simple Security check
    check_ajax_referer('pre_publish_validation', 'security');

    //check if tag is there
    if (!isset(($_GET['product_type1']))) {
        //some other product i dont expect
        wp_send_json_success();
        return true;
    }

    //read tag value
    $product_type = $_GET['product_type1'];

    //tag is there but its not a kanzu event
    if ($product_type != "simple_event") {
        //some other product i dont expect
        wp_send_json_success();
        return true;
    }

    if (!isset(($_GET['txt_start_date1']))) {
        //error
        $msg = 'You have choosen to create a Kanzu Event. Please Supply a Start Date For the Event';

        WriteToLog($msg);

        //build response
        $data = array(
            'message' => __($msg),
        );
        //return error message
        wp_send_json_error($data);
        return false;
    }

    if (!isset($_GET['txt_end_date1'])) {
        //cosntruct error message
        $msg = 'You have choosen to create a Kanzu Event. Please Supply an End Date For the Event';

        //log it
        WriteToLog($msg);

        //build response
        $data = array(
            'message' => __($msg),
        );

        //return error message
        wp_send_json_error($data);

        return false;
    }

    $start_date =  $_GET['txt_start_date1'];
    $end_date =  $_GET['txt_end_date1'];

    if (empty(($start_date))) {
        //error
        $msg = 'You have choosen to create a Kanzu Event. Please Supply a Start Date For the Event';

        WriteToLog($msg);

        //build response
        $data = array(
            'message' => __($msg),
        );
        //return error message
        wp_send_json_error($data);
        return false;
    }

    if (empty(($end_date))) {
        //cosntruct error message
        $msg = 'You have choosen to create a Kanzu Event. Please Supply an End Date For the Event';

        //log it
        WriteToLog($msg);

        //build response
        $data = array(
            'message' => __($msg),
        );

        //return error message
        wp_send_json_error($data);

        return false;
    }

    //success
    wp_send_json_success();
    return true;
}


//writes stuff out to debug.log
if (!function_exists('WriteToLog')) {
    function WriteToLog($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}
